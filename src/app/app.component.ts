import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EmethValidators} from "@ceneval_plataforma_emeth/ngx-emeth";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'capacitacion';
  form: FormGroup<TipoForm>;
  minimoAcronimo = 3;

  constructor() {
    this.form = new FormGroup<TipoForm>({
      acronimo: new FormControl<string>('', {
        validators: [
          EmethValidators.notBlank,
          Validators.minLength(this.minimoAcronimo)
        ], nonNullable: true
      }),
      nombre: new FormControl<string>('', {nonNullable: true}),
      vigenciaInicio: new FormControl<string>('', {nonNullable: true}),
      vigenciaFin: new FormControl<string>('', {nonNullable: true})
    });
  }

  ngOnInit() {
  }

  guardar() {
    this.form.markAllAsTouched();
    if(this.form.invalid)
      return;
    console.log('EPSAE guardada');
    console.log(this.form.getRawValue());
  }
}

type TipoForm = {
  acronimo: FormControl<string>;
  nombre: FormControl<string>;
  vigenciaInicio: FormControl<string>;
  vigenciaFin: FormControl<string>;
}
